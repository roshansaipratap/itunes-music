import React, { useContext, useState } from "react";
import { SongContext } from "../SongContext";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { faHeart as faHeartRegular } from "@fortawesome/free-regular-svg-icons";

const Song = (props) => {
  const { song } = props;
  const { addFavorite, removeFavorite, favorites } = useContext(SongContext);
  const [isFavorite, setIsFavorite] = useState(true);

  const submitFavorite = () => {
    setIsFavorite(!isFavorite);
    if (isFavorite) {
      addFavorite(song);
    } else {
      removeFavorite(song);
    }
  };

  return (
    <React.Fragment>
      <div className="card" onClick={props.onClick}>
        <FontAwesomeIcon
          onClick={submitFavorite}
          icon={
            favorites.find((item) => {
              return item["im:name"].label === song["im:name"].label;
            })
              ? faHeart
              : faHeartRegular
          }
        />
        <Link to={`/${song.id.attributes["im:id"]}`}>
          <div className="card-inner">
            <div className="card-front">
              <img className="image" src={song["im:image"][2].label} alt="" />
            </div>
            <div className="card-back">
              <h1>{song["im:name"].label}</h1>
              <ul>
                <li>
                  <strong>Artist:</strong> {song["im:artist"].label}
                </li>
                <li>
                  <strong>Category:</strong> {song.category.attributes.term}
                </li>
                <li>
                  <strong>Release Date:</strong>{" "}
                  {song["im:releaseDate"].attributes.label}
                </li>
                <li>
                  <strong>Price:</strong> {song["im:price"].label}
                </li>
              </ul>
            </div>
          </div>
        </Link>
      </div>
    </React.Fragment>
  );
};

export default Song;
