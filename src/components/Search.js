import React, { useState, useContext } from "react";
import { SongContext } from "../SongContext";

const Search = () => {
  const { setSearch } = useContext(SongContext);
  const [text, setText] = useState("");
  // const { type, setType } = useState("Name");
  const [category, setCategory] = useState("Name");

  const types = ["Name", "Artist", "Category", "Favorites"];

  const OnChangeText = (value) => {
    setText(value);
    setSearch(value, category);
  };

  const OnChangeType = (value) => {
    setCategory(value);
    setSearch(text, value);
  };

  return (
    <section className="search">
      <form className="inline-form">
        <input
          type="text"
          placeholder="Search..."
          value={text}
          onChange={(e) => OnChangeText(e.target.value)}
          autoFocus
        />
        <select
          value={category}
          id="type"
          onChange={(e) => OnChangeType(e.target.value)}
        >
          {types.map((item) => (
            <option key={item} value={item}>
              {item}
            </option>
          ))}
        </select>
      </form>
    </section>
  );
};

export default Search;
