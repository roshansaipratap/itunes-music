import React, { useContext } from "react";
import { SongContext } from "../SongContext";
import Header from "./Header";
import Songs from "./Songs";
import Search from "./Search";
import Favs from "./Favs";

const Home = () => {
  const { categoryValue } = useContext(SongContext);
  if (categoryValue === "Favorites") {
    return (
      <React.Fragment>
        <Header />
        <Search />
        <Favs />
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Header />
        <Search />
        <Songs />
      </React.Fragment>
    );
  }
};
export default Home;
