import React, { useContext } from "react";
import { SongContext } from "../SongContext";
import Song from "./Song";

const Songs = () => {
  const { songs } = useContext(SongContext);
  return (
    <section className="cards">
      {songs.map((song) => (
        <Song key={song.id.label} song={song}></Song>
      ))}
    </section>
  );
};

export default Songs;
