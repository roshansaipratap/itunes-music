import React from "react";

const Header = () => {
  return (
    <header className="center">
      <h1 className="header-title">Top 100 Albums</h1>
    </header>
  );
};

export default Header;
