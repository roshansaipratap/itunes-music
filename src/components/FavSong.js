import React from "react";

const FavSong = (props) => {
  const { song } = props;
  return (
    <React.Fragment>
      <div className="card" onClick={props.onClick}>
        <div className="card-inner">
          <div className="card-front">
            <img className="image" src={song["im:image"][2].label} alt="" />
          </div>
          <div className="card-back">
            <h1>{song["im:name"].label}</h1>
            <ul>
              <li>
                <strong>Artist:</strong> {song["im:artist"].label}
              </li>
              <li>
                <strong>Category:</strong> {song.category.attributes.term}
              </li>
              <li>
                <strong>Release Date:</strong>{" "}
                {song["im:releaseDate"].attributes.label}
              </li>
              <li>
                <strong>Price:</strong> {song["im:price"].label}
              </li>
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default FavSong;
