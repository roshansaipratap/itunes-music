import React from "react";
import Spinner from "./Spinner";
import { Link } from "react-router-dom";

export default function SongDetailView(props) {
  const { song } = props;
  if (song !== undefined) {
    return (
      <div
        className="row"
        style={{ alignContent: "center", alignItems: "center" }}
      >
        <div style={{ textAlign: "center" }}>
          <img
            className="detailimage"
            src={song["im:image"][2].label}
            alt={song["im:name"].label}
          />
        </div>
        <h1 className="namelabel">{song["im:name"].label}</h1>
        <h5 className="artistlabel">{song["im:artist"].label}</h5>
        <p className="label">{song.category.attributes.term}</p>
        <p className="label">{song["im:releaseDate"].attributes.label}</p>
        <div style={{ textAlign: "center" }}>
          <Link to={"/"}>
            <button className="button" onClick={props.onClick}>
              Back
            </button>
          </Link>
        </div>
      </div>
    );
  } else {
    return <Spinner />;
  }
}
