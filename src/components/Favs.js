import React, { useContext } from "react";
import { SongContext } from "../SongContext";
import FavSong from "./FavSong";
import Spinner from "./Spinner";

const Favs = () => {
  const { songs, isLoading } = useContext(SongContext);
  if (songs.length === 0) {
    return <h1 className="nolabel">No Favorites</h1>;
  } else {
    return isLoading ? (
      <Spinner />
    ) : (
      <section className="cards">
        {songs.map((song) => (
          <FavSong key={song.id.label} song={song}></FavSong>
        ))}
      </section>
    );
  }
};

export default Favs;
