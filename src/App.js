import React, { useContext } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import SongContextProvider from "./SongContext";
import { SongContext } from "./SongContext";
import SongDetail from "./components/SongDetail";
import Favs from "./components/Favs";
import Home from "./components/Home";

function App() {
  const SongWithId = ({ match }) => {
    const { songs } = useContext(SongContext);
    return (
      <SongDetail
        song={
          songs.filter(
            (song) => song.id.attributes["im:id"] === match.params.id
          )[0]
        }
      />
    );
  };

  return (
    <div className="App">
      <SongContextProvider>
        <div className="container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/hello" component={Favs} />
            <Route exact path="/:id" component={SongWithId} />
          </Switch>
        </div>
      </SongContextProvider>
    </div>
  );
}

export default App;
