import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

export const SongContext = createContext();

const SongContextProvider = (props) => {
  // const favSongs = JSON.parse(localStorage.getItem("favos"));
  const [songs, setSongs] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchvalue, setSearchValue] = useState("");
  const [categoryValue, setCategoryValue] = useState("");

  const setSearch = (svalue, cvalue) => {
    setSearchValue(svalue);
    setCategoryValue(cvalue);
  };

  const removeFavorite = (value) => {
    if (
      favorites.find((item) => {
        return item["im:name"].label === value["im:name"].label;
      })
    ) {
      setFavorites(
        favorites.filter(
          (favorite) => favorite["im:name"].label !== value["im:name"].label
        )
      );
    }
  };

  const addFavorite = (value) => {
    if (
      !favorites.find((item) => {
        return item["im:name"].label === value["im:name"].label;
      })
    ) {
      setFavorites([...favorites, value]);
    }
  };

  useEffect(() => {
    axios
      .get("https://itunes.apple.com/us/rss/topalbums/limit=100/json")
      .then(function (response) {
        const data = response.data.feed.entry;
        setSongs(data);
        setIsLoading(false);
        if (categoryValue === "Favorites") {
          setSongs(favorites);
        } else if (searchvalue) {
          let filteredSongs = [];
          filteredSongs = data.filter((song) => {
            if (categoryValue === "Name") {
              return song["im:name"].label
                .toLowerCase()
                .includes(searchvalue.toLowerCase());
            } else if (categoryValue === "Artist") {
              return song["im:artist"].label
                .toLowerCase()
                .includes(searchvalue.toLowerCase());
            } else if (categoryValue === "Category") {
              return song.category.attributes.label
                .toLowerCase()
                .includes(searchvalue.toLowerCase());
            } else {
              return song["im:name"].label
                .toLowerCase()
                .includes(searchvalue.toLowerCase());
            }
          });
          setSongs(filteredSongs);
        }
      });
  }, [searchvalue, categoryValue, favorites]);

  return (
    <SongContext.Provider
      value={{
        songs,
        isLoading,
        setSearch,
        addFavorite,
        removeFavorite,
        favorites,
        categoryValue,
      }}
    >
      {props.children}
    </SongContext.Provider>
  );
};

export default SongContextProvider;
