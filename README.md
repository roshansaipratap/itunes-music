Hello Andy & Molly,

First of all, I would like to thank you people for giving me this opportunity due to which I have able to learn some new things and also able to perform and showcase my skills.

I have done this project using React library. I have used CSS, ReactStrap, JSX for building Html as well as styling.

JSX stands for JavaScript XML. It allows us to write HTML in React. It makes it easier to write and add HTML in React.

Generally we use React Redux for managing the state of our application. Redux is used for complex applications where we have multiple components and one component is dependant on other component's state. As part of this application, I have used React Hooks which has been added to react 16.8. It's a recent feature for local state management.

On launching the application, you will see 100 albums loaded where you can search with 'Name' as default

Apart from that, I have also worked on 2 bonus questions which you have mentioned in the document. 

1) Song Detail Component - When you click on one of the albums, it gets routed to a new page where you can see the preview of that album.

2) I have added a couple of new features as part of bonus 2.

   a) Advanced Search - This is the place where you can search with either name, artist or category of the album by selecting the value from the dropdown.

   b) Favorites - You can make your favorite album selected on top of your album and view your favorite albums by selecting the favorites in the dropdown.

This project was bootstrapped with Create React App.

To Run the Project :

1) Download the project from my repository.
2) Go to itunes-music folder in the project and from their open command prompt.
3) Run command (npm install) to download all the modules you need to download as part of the project. 


Available Scripts
4) In the project directory, you can run:

(npm start)
Runs the app in the development mode. Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.You will also see any lint errors in the console.